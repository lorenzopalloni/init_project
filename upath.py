from path import Path
import inspect
import sys
import argparse
import os

PROJECT_DIR = Path(inspect.getsourcefile(lambda: 0)).abspath().parent
PROJECTS_DIR = PROJECT_DIR.parent
WORKAROUND_DIR = Path(sys.prefix) / 'lib/python3.6/site-packages/'
WORKAROUND_PATH = WORKAROUND_DIR / 'workaround.pth'

parser = argparse.ArgumentParser()
parser.add_argument('--add_models_path', '-m', type=bool, default=False,
                    help="add the ./src/models path within interpreter paths")
parser.add_argument('--project_name', '-p', type=str, default=PROJECT_DIR.name,
					help="specify a project name")
flags = parser.parse_args()

with open(str(WORKAROUND_PATH), 'w') as f:
    f.write(str(PROJECTS_DIR / flags.project_name))

    if flags.add_models_path:
        f.write(os.pathsep + str(PROJECTS_DIR / project_name / 'src' / 'models'))

print(">>> Python path updated.")
print(">>> Project name: {0}".format(PROJECT_DIR))
print(">>> Hint: If the project name doesn't match, relauch this program specifying the project name.")
