from path import Path
import inspect
import os

# this script assume that this file is located in /home/<user>/PycharmProjects/<project_name>/src/constants/
PROJECT = Path(inspect.getsourcefile(lambda: 0)).abspath().parent.parent.parent


def validate_project_path(project_path=PROJECT):
    input_string = 'Please insert the path to project directory:\n'
    if project_path.parent != Path(os.environ['HOME']) / 'PycharmProjects':
        project_path = Path(input(input_string))
    return project_path


PROJECT = validate_project_path()

# defining all the project folders
DATA = PROJECT / 'data'
RAW = DATA / 'raw'
INTERIM = DATA / 'interim'
PROCESSED = DATA / 'processed'

DOCS = PROJECT / 'docs'

NOTEBOOKS = PROJECT / 'notebooks'

OUTPUTS = PROJECT / 'outputs'
PLOTS = OUTPUTS / 'plots'
SUBMISSIONS = OUTPUTS / 'submissions'

SRC = PROJECT / 'src'
CONSTANTS = SRC / 'constants'
MAKE_DATA = SRC / 'make_data'
MODELS = SRC / 'models'

HOME = Path(os.environ['HOME'])
KAGGLE = HOME / '.kaggle'
COMPETITIONS = KAGGLE / 'competitions'
COMPETITION_FOLDER = COMPETITIONS / 'example-competition'  # TO CHANGE WITH CURRENT COMPETITION NAME

if __name__ == "__main__":

    print("setting up project structure in", str(PROJECT))
    dir_list = [PROJECT, DATA, RAW, INTERIM, PROCESSED, DOCS, NOTEBOOKS, OUTPUTS,
                PLOTS, SUBMISSIONS, SRC, CONSTANTS, MAKE_DATA, MODELS]

    for directory in dir_list:
        directory.mkdir_p()
